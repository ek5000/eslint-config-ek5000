const ERROR = 'error';
const STYLE = 'warn';
// const WARN = 'warn';
const OFF = 'off';

// eslint-disable-next-line import/no-commonjs,unicorn/prefer-module
module.exports = {
    env: {
        browser: true,
        es6: true,
        es2022: true,
    },
    extends: [
        'airbnb',
        'airbnb/hooks',
        'plugin:unicorn/recommended',
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    plugins: [
        'unicorn',
    ],
    rules: {
        'indent': [STYLE, 4, { SwitchCase: 1 }],
        'no-async-promise-executor': ERROR,
        'no-misleading-character-class': ERROR,
        'require-atomic-updates': ERROR,
        'max-classes-per-file': [ERROR, 1],
        'no-self-assign': ERROR,
        'import/default': ERROR,
        'import/no-commonjs': ERROR,
        'import/no-namespace': ERROR,
        'import/no-internal-modules': OFF,
        'array-bracket-newline': [ERROR, 'consistent'],
        'array-element-newline': [ERROR, 'consistent'],
        'func-name-matching': ERROR,
        'func-names': ERROR,
        'func-style': [ERROR, 'declaration'],
        'jsx-quotes': [ERROR, 'prefer-double'],
        'no-nested-ternary': OFF,
        'no-plusplus': OFF,
        'no-restricted-syntax': [
            ERROR,
            {
                selector: 'LabeledStatement',
                message: 'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.',
            },
            {
                selector: 'WithStatement',
                message: '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.',
            },
        ],
        'object-curly-newline': [ERROR, { consistent: true }],
        'operator-linebreak': OFF,
        'prefer-object-spread': ERROR,
        'quote-props': [ERROR, 'consistent-as-needed'],
        'no-use-before-define': [ERROR, { functions: false, classes: true, variables: true }],
        'unicorn/custom-error-definition': ERROR,
        'react/jsx-indent': [ERROR, 4],
        'unicorn/prevent-abbreviations': OFF,
        'react/jsx-filename-extension': OFF,
        'react/react-in-jsx-scope': OFF,
        'react/jsx-indent-props': [ERROR, 4],
        'react/jsx-props-no-spreading': OFF,
        'max-len': [ERROR, { code: 160 }],
        'unicorn/no-null': OFF,
    },
    overrides: [
        {
            files: ['*.test.js'],
            env: {
                jasmine: true,
                jest: true,
            },
        },
    ],
};
